# builtins
import os
import json
import asyncio
import webbrowser
import pkg_resources
from functools import partial
from datetime import datetime, timezone, timedelta
from typing import List, Dict, Any, Generator, Callable

# 3rd party
from trello import TrelloApi

# Internal
from modifiers.modifier import Modifier
from utils.search_utils import get_board, get_list, get_card


class DailyRoutine(TrelloApi):
    def __init__(self, config, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._username = config["user"]["username"]
        self._board_config = config["board"]
        self._logging_config = config["logging"]
        self._modifiers_config = config["modifiers"]

        self._modifiers = self._load_modifiers()

        if not self._token:
            self.ask_authorization()

    @property
    def username(self):
        return self._username

    async def ask_authorization(self):
        auth_url = self.get_token_url(app_name="Daily Routine", expires="never")
        print(f"You need to authorize app usage of your user: {self.username}, than restart the app")
        await asyncio.sleep(1.5)
        webbrowser.open_new_tab(auth_url)

    # region progress logging
    async def log_progress(self):
        progress_loggers = {
            routine_name: asyncio.Task(self._get_routine_progress(routine_tasks["tasks_card"]))
            for routine_name, routine_tasks
            in self._board_config["tasks"].items()
        }

        await asyncio.gather(*progress_loggers.values())

        progress_log = {
            routine_name: routine_progress_coro.result()
            for routine_name, routine_progress_coro
            in progress_loggers.items()
        }
        return progress_log

    async def _get_routine_progress(self, tasks_card):
        board_id = (await get_board(self, self.username, self._board_config["name"]))["id"]
        tasks_list = await get_list(self, board_id, self._board_config["tasks_list"])

        routine_tasks_card = await get_card(self, tasks_list["id"], tasks_card)
        if routine_tasks_card:
            tasks_card_checklist_id = routine_tasks_card["idChecklists"][0]
        else:
            raise RuntimeWarning(f"Task card checklist for card '{tasks_card}' not found")

        checklist_items = self.checklists.get_checkItem(tasks_card_checklist_id)
        return {
            "description": routine_tasks_card["desc"],
            "tasks": {task["name"]: task["state"] for task in checklist_items}
        }

    def save_progress(self, progress_log, file_path=None):
        if not file_path:
            file_path = self._logging_config["progress_log_path"]

        with open(file_path, "w") as progress_file:
            json.dump(progress_log, progress_file)
    # endregion

    # region monthly tasks
    async def create_month_tasks(self):
        month_duedate = self._get_today_duedate()
        month_duedate = month_duedate.replace(
            month=month_duedate.month+1,
            day=1)
        month_duedate -= timedelta(days=1)

        monthly_config = self._board_config["tasks"]["monthly"]
        board_id = (await get_board(self, self.username, self._board_config["name"]))["id"]
        await self._create_tasks_card(board_id, monthly_config, month_duedate)
    # endregion

    # region weekly tasks
    async def create_week_tasks(self):
        weekend_duedate = self._get_today_duedate()
        weekend_duedate += timedelta(days=self.__calc_distance_to_weekend(weekend_duedate.weekday()))

        weekly_config = self._board_config["tasks"]["weekly"]
        board_id = (await get_board(self, self.username, self._board_config["name"]))["id"]
        await self._create_tasks_card(board_id, weekly_config, weekend_duedate)

    @staticmethod
    def __calc_distance_to_weekend(weekday: int) -> int:
        return 5 - ((weekday % 6) - weekday // 6)
    # endregion

    # region daily tasks
    async def create_day_tasks(self):
        today_duedate = self._get_today_duedate()

        daily_config = self._board_config["tasks"]["daily"]
        board_id = (await get_board(self, self.username, self._board_config["name"]))["id"]
        await self._create_tasks_card(board_id, daily_config, today_duedate)
    # endregion

    @staticmethod
    def _get_today_duedate():
        return datetime.now(timezone(offset=timedelta(hours=3))).replace(
            hour=23,
            minute=59,
            second=0,
            microsecond=0)

    # region tasks helper functions
    async def _create_tasks_card(self, board_id: str, task_config: Dict[str, str], due_date: datetime):
        tasks_list = await get_list(self, board_id, self._board_config["tasks_list"])

        task_card = await get_card(self, tasks_list["id"], task_config["tasks_card"])
        if task_card:
            card_duedate = datetime.strptime(task_card["due"], "%Y-%m-%dT%H:%M:%S.%f%z")
            if card_duedate and card_duedate.date() == due_date.date():
                return
        else:
            task_card = self.cards.new(task_config["tasks_card"], tasks_list["id"])

        goal_cards_list = await get_list(self, board_id, task_config["goals_list"])
        await self._config_card_tasks(task_card["id"], goal_cards_list["cards"], due_date)

    async def _config_card_tasks(self, task_card_id: str, goal_cards: List[Dict[str, Any]], due_date: datetime):
        self.cards.update(task_card_id, due=due_date.isoformat(), desc="")
        card_checklists = self.cards.get_checklist(task_card_id)

        for checklist in card_checklists:
            self.cards.delete_checklist_idChecklist(checklist["id"], task_card_id)

        tasks_checklist = self.checklists.new(task_card_id, name="Tasks")
        for card in goal_cards:
            await self._add_task_item(tasks_checklist["id"], card)

    async def _add_task_item(self, checklist_id: str, goal_card: Dict[str, Any]):
        checklist_item = self.checklists.new_checkItem(checklist_id, name=goal_card["name"])
        task_card = self.cards.get(self.checklists.get(checklist_id)["idCard"])

        for modifier in self._get_task_modifiers(goal_card["desc"]):
            await modifier.modify(checklist_task_item=checklist_item, task_card=task_card, goal_card=goal_card)
    # endregion

    # region modifiers helper functions
    def _get_task_modifiers(self, card_desc: str) -> Generator[Modifier, None, None]:
        for line in card_desc.splitlines():
            if not line.startswith("@"):
                continue

            modifier = self._get_modifier(line[1:])
            if modifier:
                yield modifier

    def _get_modifier(self, modifier_request: str) -> Modifier:
        request_args_index = modifier_request.find("(")
        if request_args_index == -1:
            modifier_name, modifier_args, modifier_kwargs = modifier_request, [], {}
        else:
            modifier_name = modifier_request[:request_args_index]
            modifier_args = map(str.strip, modifier_request[request_args_index + 1:-1].split(","))
            # arg.split will always give a list with len = 2
            # this is insured by the check for the separator in `arg` and `maxsplit` equal to 1
            # noinspection PyTypeChecker
            modifier_kwargs = dict(map(str.strip, arg.split("=", 1)) for arg in modifier_args if "=" in arg)

        modifier = self._modifiers.get(modifier_name)
        if modifier:
            return modifier(*modifier_args, **modifier_kwargs)

    def _load_modifiers(self) -> Dict[str, Callable]:
        return {
            entry_point.name: partial(entry_point.load(), api=self, config=self._modifiers_config.get(entry_point.name))
            for entry_point
            in pkg_resources.iter_entry_points("daily_routine.modifiers")
        }
    # endregion


async def main():
    with open("config.json", "r") as config_file:
        config = json.load(config_file)
    daily_routine = DailyRoutine(
        config=config,
        apikey=os.environ.get("TRELLO_APP_KEY"),
        token=os.environ.get("MY_TOKEN"))

    daily_routine.save_progress(await daily_routine.log_progress())
    await asyncio.gather(
        daily_routine.create_day_tasks(),
        daily_routine.create_week_tasks(),
        daily_routine.create_month_tasks()
    )


if __name__ == '__main__':
    asyncio.run(main())
