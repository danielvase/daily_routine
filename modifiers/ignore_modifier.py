# builtin
from typing import TYPE_CHECKING, Dict, Any

# internal
from modifiers.modifier import Modifier

if TYPE_CHECKING:
    from daily_routine import DailyRoutine


class IgnoreModifier(Modifier):
    def __init__(self, api: 'DailyRoutine', config: Dict[str, Any]):
        super().__init__(api, config)

    async def _modify(self, checklist_task_item: Dict[str, Any], task_card: Dict[str, Any], goal_card: Dict[str, Any]):
        self._api.checklists.delete_checkItem_idCheckItem(checklist_task_item["id"], checklist_task_item["idChecklist"])
        await self._clear_desc(task_card, goal_card["name"])
        await self._flag_card(goal_card)
