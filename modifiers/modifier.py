# builtins
from typing import Dict, Any, TYPE_CHECKING
from abc import ABCMeta, abstractmethod

if TYPE_CHECKING:
    from daily_routine import DailyRoutine


class Modifier(metaclass=ABCMeta):
    FLAG_LABEL_COLOR = "red"
    FLAG_LABEL_NAME = "flagged"

    def __init__(self, api: 'DailyRoutine', config: Dict[str, Any]):
        self._config = config
        self._api = api

    async def modify(self, checklist_task_item: Dict[str, Any], task_card: Dict[str, Any], goal_card: Dict[str, Any]):
        if not await self.__is_flagged(goal_card):
            await self._modify(checklist_task_item, task_card, goal_card)

    async def __is_flagged(self, card: Dict[str, Any]):
        for label in card["labels"]:
            if label["color"] == self.FLAG_LABEL_COLOR and label["name"] == self.FLAG_LABEL_NAME:
                return True
        return False

    @abstractmethod
    async def _modify(self, checklist_task_item: Dict[str, Any], task_card: Dict[str, Any], goal_card: Dict[str, Any]):
        pass

    async def _flag_card(self, card: Dict[str, Any]):
        if not await self.__is_flagged(card):
            self._api.cards.new_label(card["id"], self.FLAG_LABEL_COLOR, self.FLAG_LABEL_NAME)

    async def _add_desc(self, task_card: Dict[str, Any], goal_name: str, desc: str):
        old_desc = task_card["desc"]
        self._api.cards.update_desc(task_card["id"], f"{old_desc}{goal_name}: {desc}\n")

    async def _clear_desc(self, task_card: Dict[str, Any], goal_name: str):
        cleared_desc = "\n".join(line for line in task_card["desc"].splitlines() if not line.startswith(goal_name))
        self._api.cards.update_desc(task_card["id"], cleared_desc)
