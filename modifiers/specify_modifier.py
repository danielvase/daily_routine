# builtins
import random
from typing import List, Dict, Any, TYPE_CHECKING

# internal
from modifiers.modifier import Modifier
from utils.search_utils import get_board, get_list

if TYPE_CHECKING:
    from daily_routine import DailyRoutine


class SpecifyModifier(Modifier):
    def __init__(self, api: 'DailyRoutine', config: Dict[str, Any], choice_type: str = "random",
                 choices_board: str = "backlog", choices_list: str = None, done_board: str = None,
                 done_list: str = None, save: str = "False"):
        super().__init__(api, config)

        self._choices_board_name = config.get("boards").get(choices_board, choices_board)
        self._choices_list_name = choices_list
        self._done_board_name = done_board
        self._done_list_name = done_list
        self._choice_type = choice_type
        self._save_after_chosen = True if save == "True" else False

    async def _modify(self, checklist_task_item: Dict[str, Any], task_card: Dict[str, Any], goal_card: Dict[str, Any]):
        choices_board = await get_board(self._api, self._api.username, self._choices_board_name)
        choices_list_name = self._choices_list_name if self._choices_list_name else goal_card["name"]
        choices_list = await get_list(self._api, choices_board["id"], choices_list_name)

        if not choices_list or not choices_list["cards"]:
            return
        chosen_card = self._choose_card(choices_list["cards"])

        update_desc = f"{chosen_card['name']}({chosen_card['desc'][:-1]})"
        await self._add_desc(task_card, goal_card["name"], update_desc)

        if self._save_after_chosen:
            await self._save_card(choices_board, choices_list, chosen_card["id"], self._done_board_name, self._done_list_name)
        else:
            self._api.cards.update_closed(chosen_card["id"], True)

    def _choose_card(self, cards: List[Dict[str, Any]]) -> Dict[str, Any]:
        if self._choice_type == "priority":
            return cards[0]
        else:
            return random.choice(cards)

    async def _save_card(self, choices_board: Dict[str, Any], choices_list: Dict[str, Any], chosen_card_id: str,
                         save_board_name: str, save_list_name: str):
        if not save_list_name:
            save_list_name = f"{choices_list['name']} - Done"
        if save_board_name:
            save_board_id = await get_board(self._api, self._api.username, save_board_name)
        else:
            save_board_id = choices_board["id"]

        save_list = await get_list(self._api, save_board_id, save_list_name)
        if not save_list:
            save_list = self._api.lists.new(save_list_name, choices_board["id"], pos=choices_list["pos"])

        self._api.cards.update_idList(chosen_card_id, save_list["id"])
