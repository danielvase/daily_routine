from setuptools import setup

setup(
    name='daily_routine',
    entry_points={
        "daily_routine.modifiers": [
            "specify = modifiers.specify_modifier:SpecifyModifier",
            "ignore = modifiers.ignore_modifier:IgnoreModifier"
        ]
    }
)
