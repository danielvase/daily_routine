# builtins
from typing import Dict, Any, TYPE_CHECKING

if TYPE_CHECKING:
    from trello import TrelloApi


async def get_board(api: 'TrelloApi', username: str, board_name) -> Dict[str, Any]:
    user_boards = api.members.get_board(username, filter="open")
    for board in user_boards:
        if board["name"] == board_name:
            return board


async def get_list(api: 'TrelloApi', board_id: str, list_name: str) -> Dict[str, Any]:
    board_lists = api.boards.get_list(board_id, cards="open")
    for brd_list in board_lists:
        if brd_list["name"] == list_name:
            return brd_list


async def get_card(api: 'TrelloApi', list_id: str, card_name: str) -> Dict[str, Any]:
    for card in api.lists.get_card(list_id):
        if card["name"] == card_name:
            return card
